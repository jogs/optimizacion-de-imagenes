const dimentionsOf = require('image-size')
const {
    statSync,
    writeFileSync
} = require('fs')
const readdir = require('recursive-readdir')
const {
    resolve,
    join
} = require('path')

const isImage = source => /\.(png|jpg)$/.test(source)

const compare = (reductionPercent) => reductionPercent > 10 ? '👍🏻' : '👎🏻'

const imgData = imgPath => {
    let size = statSync(imgPath).size
    let kbSize = (size / 1024).toFixed(1)
    let imgSize = dimentionsOf(imgPath)
    return {
        type: imgSize.type,
        size,
        kbSize,
        width: imgSize.width,
        height: imgSize.height,
        dimentions: `${imgSize.width}x${imgSize.height}`
    }
}

const src = resolve(__dirname, 'src', 'img')
const dist = resolve(__dirname, 'dist', 'img')

readdir(src).then(files => {
    let data = files.filter(isImage).map(srcfile => {
        let file = srcfile.replace(src, '')
        let distfile = join(dist, file)
        let original = imgData(srcfile)
        let compressed = {
            size: 0
        }
        try {
            compressed = imgData(distfile)
        } catch (error) {
            console.error(distfile, error)
        } finally {
            let reduction = ((1 - (compressed.size / original.size)) * 100)
            return {
                path: file,
                compressed,
                original,
                evaluateCompressed: compare(reduction),
                reductionPercent: reduction.toFixed(2)
            }
        }
    })
    writeFileSync(join(dist, '..', 'resumen.json'), JSON.stringify(data, null, 2))
    console.log('FIN')
})