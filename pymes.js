let test = /([^/]+)\/(.+)\.(jpg|png)$/

module.exports = [
    {
        from: "src\\img",
        to: "img",
        test
    },
    {
        from: "src\\img\\blog",
        to: "img\\blog"
    },
    {
        from: "src\\img\\crossell",
        to: "img\\crossell"
    },
    {
        from: "src\\img\\blog",
        to: "img\\blog"
    },
    {
        from: "src\\img\\financiamiento\\desktop",
        to: "img\\financiamiento\\desktop"
    },
    {
        from: "src\\img\\financiamiento\\mobile",
        to: "img\\financiamiento\\mobile"
    },
    {
        from: "src\\img\\financiamiento\\landscape",
        to: "img\\financiamiento\\landscape"
    },
    {
        from: "src\\img\\financiamiento\\portrait",
        to: "img\\financiamiento\\portrait"
    },
    {
        from: "src\\img\\inmuebles",
        to: "img\\inmuebles"
    },
    {
        from: "src\\img\\inmuebles\\cards",
        to: "img\\inmuebles\\cards"
    },
    {
        from: "src\\img\\inmuebles\\desktop",
        to: "img\\inmuebles\\desktop"
    },
    {
        from: "src\\img\\inmuebles\\landscape",
        to: "img\\inmuebles\\landscape"
    },
    {
        from: "src\\img\\inmuebles\\mobile",
        to: "img\\inmuebles\\mobile"
    },
    {
        from: "src\\img\\inmuebles\\portrait",
        to: "img\\inmuebles\\portrait"
    },
    {
        from: "src\\img\\paquetePyme",
        to: "img\\paquetePyme"
    },
    // {
    //     from: "src\\img\\restaurants",
    //     to: "img\\restaurants"
    // },
    {
        from: "src\\img\\restaurants\\cards",
        to: "img\\restaurants\\cards"
    },
    {
        from: "src\\img\\restaurants\\desktop",
        to: "img\\restaurants\\desktop"
    },
    {
        from: "src\\img\\restaurants\\landscape",
        to: "img\\restaurants\\landscape"
    },
    {
        from: "src\\img\\restaurants\\mobile",
        to: "img\\restaurants\\mobile"
    },
    {
        from: "src\\img\\restaurants\\portrait",
        to: "img\\restaurants\\portrait"
    },
    {
        from: "src\\img\\soluciones",
        to: "img\\soluciones"
    },
]