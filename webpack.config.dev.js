const ImageminPlugin = require('imagemin-webpack-plugin').default
const CopyWebpackPlugin = require('copy-webpack-plugin')
const imageminMozjpeg = require('imagemin-mozjpeg')
const {
    resolve
} = require('path')
// const configExtra = require('./pymes')

let test = /([^/]+)\/(.+)\.(jpg|png)$/

module.exports = {
    entry: './src/index.js',
    output: {
        filename: 'bundle.js',
        path: resolve(__dirname, 'dist')
    },
    plugins: [
        new CopyWebpackPlugin([{
            from: "src\\img",
            to: "img",
            test
        }]),
        new ImageminPlugin({
            plugins: [
                imageminMozjpeg({
                    // de 0 a 100 donde el 0 es la peor calidad y 100 es la mejor calidad
                    quality: 80,
                    progressive: true
                })
            ]
        })
    ]
}